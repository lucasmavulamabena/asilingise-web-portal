
import { Map1Component } from './views/maps/map1/map1.component';
import { ModalsComponent } from './views/modals/modals.component';
import { BasicTableComponent } from './views/tables/basic-table/basic-table.component';
import { Profile1Component } from './views/profile/profile1/profile1.component';
import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NotFoundComponent } from './views/errors/not-found/not-found.component';
import { Dashboard1Component } from './views/dashboards/dashboard1/dashboard1.component';
import {IncidentComponent} from './views/incidents/incident/incident.component';
import {IncidentDetailsComponent} from './views/incidents/incident-details/incident-details.component';
import { IncidentOnMapComponent } from './views/incidents/incident-on-map/incident-on-map.component';


const routes: Route[] = [
  { path: '', component: IncidentComponent},
  { path: 'indident-details/:id', component: IncidentDetailsComponent},
  { path: 'indident-on-map/:id', component: IncidentOnMapComponent},  
  { path: 'incidents', component: IncidentComponent},
  { path: 'tables', children:
    [
      { path: 'table1', component: BasicTableComponent },
    ]
  },
  { path: 'maps', children:
    [
      { path: 'map1', component: Map1Component},
    ]
  },

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
