import { Injectable } from  '@angular/core';
import { HttpClient} from  '@angular/common/http';

@Injectable({
    providedIn:  'root'
})

export  class  IncidentService {
    API_URL  =  'http://10.113.11.10/Asilungise';

    constructor(private  httpClient:  HttpClient) {}
    
    getIncidents(){
        return  this.httpClient.get(`${this.API_URL}/api/Incident/Incident`);
    }

    getSingleIncident(id){
        return  this.httpClient.get(`${this.API_URL}/api/Incident/Incident?id=`+id);
    }
}