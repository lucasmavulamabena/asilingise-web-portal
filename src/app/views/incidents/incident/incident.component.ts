import { Component, OnInit } from '@angular/core';
import { IncidentService } from  '../../../service/incident.service';
// import 'rxjs/add/operator/map'

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.css']
})
export class IncidentComponent implements OnInit {
  data : Array<any> = [];  
  errorMessage : any;

  constructor(private  apiService:  IncidentService) { }

  ngOnInit() {  
    this.getIncidents(); 
  }  

  getIncidents(){    
    this.apiService.getIncidents().subscribe((data:  Array<object>) => {
       this.data  =  data;       
    });
  }
}
