import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-incident-on-map',
  templateUrl: './incident-on-map.component.html',
  styleUrls: ['./incident-on-map.component.scss']
})
export class IncidentOnMapComponent implements OnInit {
  
  name: string = 'My first AGM project';
  lat: number = -25.698040;
  lng: number = 28.416896;

  constructor() { }

  ngOnInit() {     
  }
}
