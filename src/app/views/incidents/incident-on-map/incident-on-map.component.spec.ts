import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentOnMapComponent } from './incident-on-map.component';

describe('IncidentOnMapComponent', () => {
  let component: IncidentOnMapComponent;
  let fixture: ComponentFixture<IncidentOnMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentOnMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentOnMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
