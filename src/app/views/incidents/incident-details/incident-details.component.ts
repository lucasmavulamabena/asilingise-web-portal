import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { IncidentService } from  '../../../service/incident.service';


@Component({
  selector: 'app-incident-details',
  templateUrl: './incident-details.component.html',
  styleUrls: ['./incident-details.component.css']
})
export class IncidentDetailsComponent implements OnInit {
  data : any = [];  

  model 
  constructor(private route: ActivatedRoute,
    private router: Router,private  apiService:  IncidentService) { }

    ngOnInit() {   
      this.route.params.subscribe(params => {
        const id = Number.parseInt(params['id']);    

        this.apiService.getSingleIncident(id).subscribe((data:  Array<object>) => {
          this.data  =  data[0];       
       });
    });
  }
  
}
